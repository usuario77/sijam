<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
<section>
    <article>
        <Form::open()>
            <fieldset>
            <legend>compra</legend>
                <h1>Dados basico</h1>

                <p><label for="iddata">Data:</label><input type="date" name="ndata" id="iddata" required></p>

                <p><label for="idtempo">Hora:</label><input type="time" name="ntempo" id="idtempo"required></p>

                <p><label for="idnf">Numero de nota fiscal:</label><input type="number" name="nnf" id="idnf" placeholder="numero da nota fiscal"required></p>

                <p><label for="idfornecedor">Fornecedor:</label><input type="search" name="nfornecedor" id="idfornecedor" placeholder="selecione o cadastro"></p>
                <fieldset>

                    <h2>adicionar cadastro</h2>
                    <label>Tipo:</label>
                    <select name="tp" id="idtp">
                        <option selected>Pessoa fisica</option>
                        <option>Pessoa Juridica</option>
                    </select>

                    <p><label>Nome fantasia:</label><input type="text" name="nnomef" id="idnomef" placeholder="nome fantasia"required></p>
                    <p><label>Telefone:</label><input type="number" name="ntelefone" id="idtelefone" placeholder="digite o telefone"required> <label>Celular:</label><input type="number" name="ncelular" id="idcelular" placeholder="digite o celular"required></p>
                    <p><label>Email:</label><input type="email" name="nemail" id="idemail" placeholder="email" required></p>
                    <label>Categoria:</label>
                    <select name="categoria" id="icategoria">
                        <option selected>cliente</option>
                        <option>fornecedor</option>
                    </select>
                    <h3>dados complementares</h3>
                    <p><label for="idcnpj">CNPJ:</label><input type="number" name="ncnpj" id="idcnpj" placeholder="numero do cnpj"required></p>
                    <p><label for="idrazao">Razão social:</label><input type="text" name="nrazao" id="idrazao" placeholder="nome da razão social"required></p>
                    <p><label for="idinscricoes">Inscrição estadual:</label><input type="number" name="ninscricoes" id="idinscricoes" placeholder="numero do cnpj"></p>
                    <p><label for="idinsces">Insc.Estadual:</label><input type="number" name="ninsces" id="idinsces" placeholder="inscrição estadual"></p>
                    <p><label for="idtelalter">Telefone alternativo:</label><input type="number" name="ntelalter" id="idtelalter" placeholder="digite telefone alternativo"></p>
                    <p><p>Observação:</p><textarea name="comentario" rows="7" cols="30"></textarea></p>
                    <label>Forma de contato:</label>
                    <select name="formacontato" id="idformacontato">
                        <option selected>contato direto</option>
                        <option>contato indireto</option>
                    </select>
                    <label>Situação:</label>
                    <select name="situacao" id="idsituacao">
                        <option selected>ativo</option>
                        <option>inativo</option>
                    </select>

                    <h4>endereco</h4>
                    <p><label for="idcep">CEP:</label><input type="number" name="ncep" id="idcep" placeholder="digite o cep"></p>
                    <p><label for="idlog">Logradouro:</label><input type="text" name="nlog" id="idlog" placeholder="digite o logradouro"> <label for="idnumero">Numero:</label><input type="number" name="nnumero" id="idnumero" placeholder="digite o numero"></p>
                    <p><label for="idcomplemento">Complemento:</label><input type="text" name="ncomplemento" id="idcomplemento" placeholder="digite o complemento"> <label for="idbairro">Bairro:</label><input type="text" name="nbairro" id="idbairro" placeholder="digite o bairro" required></p>
                    <p><label for="idcidade">Cidade:</label><input type="text" name="ncidade" id="idcidade" placeholder="digite a cidade"> <label for="idestado">Estado:</label><input type="text" name="nestado" id="idestado" placeholder="digite o estado"></p>

                </fieldset>
                <fieldset>
                    <h1>produtos adquiridos</h1>
                    <label>Situação:</label>
                    <select name="formapag" id="idformpag">
                        <option selected>cartão</option>
                        <option>boleto</option>
                        <option>dinheiro</option>

                    </select>
                    <p><label for="idcondicaopag">Condição:</label><input type="text" name="ncondicaopag" id="idcondicaopag" placeholder="exemplo 30,60,90">
                    
                </fieldset>
            </fieldset>

        <Form::close()>
    </article>
</section>
</body>
</html>